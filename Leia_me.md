# Syntrophy Lab

Repositório com wikis, desenhos 3D, esquemáticos, referências, licenças e códigos dos projetos do Syntrophy Lab.
Sintrophy Lab é um laboratório de tecnologias livres que pesquisa e desenvolve projetos de microgeração distribuída, fontes renovaveis, armazenamento, infraestrutura e gestão de energia.

Projetos:
- Micro Aerogeradores;
- Geradores Impressos em 3D;
