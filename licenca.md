[Português]

Primeiramente pensamos em escrever um desses longos termos e condições aqui. O que é super chato de ler e escrever. Então para facilitar, fizemos uma versão curta: Tudo é gratuito para qualquer uso (comercial ou pessoal). Nós apreciaríamos se você mencionasse nosso nome em algum lugar. Ah, e você não pode nos processar se algo der errado. Por isso seja cuidadoso! Mas para aqueles que gostam de versões longas de termos e confições, podem conferir abaixo as licenças que utilizamos:

## Avisos legais e licenciamento

- **CERN Open Hardware License v1.2**
    - Esta documentação descreve projetos de hardware aberto e livre. Os projetos de hardware estão licenciados sob termos da licença de hardware aberto do CERN OHL v.1.2. O que permite a liberdade do conhecimento para que qualquer um possa usar, estudar, modificar, distribuir, redistribuir e vender este projeto livremente, contanto que sigam os termos e condições da licença, como por exemplo, é vetada a possíbilidade de patentear o projeto ou projetos similares a este, saiba mais no texto da versão completa da licença acesse [CERN Open Hardware License v1.2](https://www.ohwr.org/projects/cernohl/wiki)
    - ![CERN OHL v1.2](https://tecnologias.libres.cc/organizacion-y-sociedad/entregaveis-emergentes-para-a-rede/raw/master/Figuras/Licen%C3%A7as/CERN_OHL_v1.2.76x80.png)
- **Creative Commons BY-SA 4.0**
    - O conteúdo desta página de wiki de projeto é distribuído sob uma licença [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). Autor: Cristthian Marafigo Arpino.
    - ![CC BY-SA](https://tecnologias.libres.cc/organizacion-y-sociedad/entregaveis-emergentes-para-a-rede/raw/master/Figuras/Licen%C3%A7as/CC_BY-SA.88x31.png)

Author: **Cristthian Marafigo Arpino**

---
    
[Catellano]
    
Primero pensamos en escribir uno de esos largos términos y condiciones aquí. Lo cual es muy aburrido de leer y escribir. Entonces, para facilitar, hicimos una versión corta: todo es gratis para cualquier uso (comercial o personal). Agradeceríamos que mencionaras nuestro nombre en alguna parte. Ah, y no puedes demandarnos si algo sale mal. ¡Así que ten cuidado! Pero para quienes gustan de las versiones largas de los términos y condiciones, consulte las licencias que usamos a continuación:

## Avisos legales y licencias

- **CERN Open Hardware License v1.2**
    - Esta documentación describe proyectos de hardware abiertos y gratuitos. Los proyectos de hardware están licenciados bajo la licencia de hardware abierto CERN OHL v.1.2. Esto permite la libertad de conocimiento para que cualquiera pueda usar, estudiar, modificar, distribuir, redistribuir y vender este proyecto libremente, siempre que cumplan los términos y condiciones de la licencia, como la posibilidad de patentar el proyecto o proyectos similares a este, obtenga más información en el texto de la versión completa del acceso a la licencia [CERN Open Hardware License v1.2] (https://www.ohwr.org/projects/cernohl/wiki)
    - ![CERN OHL v1.2](https://tecnologias.libres.cc/organizacion-y-sociedad/entregaveis-emergentes-para-a-rede/raw/master/Figuras/Licen%C3%A7as/CERN_OHL_v1.2.76x80.png)
- **Creative Commons BY-SA 4.0**
    - El contenido de la página wiki de este proyecto se distribuye bajo una licencia [Creative Commons BY-SA 4.0] (https://creativecommons.org/licenses/by-sa/4.0/). Autor: Cristthian Marafigo Arpino.
    - ![CC BY-SA](https://tecnologias.libres.cc/organizacion-y-sociedad/entregaveis-emergentes-para-a-rede/raw/master/Figuras/Licen%C3%A7as/CC_BY-SA.88x31.png)

Autor: **Cristthian Marafigo Arpino**

---

[English]

First we think of writing one of those long terms and conditions here. Which is super boring to read and write. So for ease, we made a short version: Everything is free for any use (commercial or personal). We would appreciate if you mentioned our name somewhere. Oh, and you can't sue us if something goes wrong. So be careful! But for those who like long versions of terms and terms, check out the licenses we use below:

## Legal Notices and Licensing

- **CERN Open Hardware License v1.2**
    - This documentation describes open and free hardware projects. Hardware projects are licensed under CERN OHL v.1.2 open hardware license. This allows freedom of knowledge for anyone to use, study, modify, distribute, redistribute and sell this project freely, as long as they follow the license terms and conditions, such as the possibility of patenting the project or projects similar to this one, learn more in the full version text of the license access [CERN Open Hardware License v1.2] (https://www.ohwr.org/projects/cernohl/wiki)
    - ![CERN OHL v1.2](https://tecnologias.libres.cc/organizacion-y-sociedad/entregaveis-emergentes-para-a-rede/raw/master/Figuras/Licen%C3%A7as/CERN_OHL_v1.2.76x80.png)
- **Creative Commons BY-SA 4.0**
    - The content of this project wiki page is distributed under a license [Creative Commons BY-SA 4.0] (https://creativecommons.org/licenses/by-sa/4.0/). Author: Cristthian Marafigo Arpino.
    - ![CC BY-SA](https://tecnologias.libres.cc/organizacion-y-sociedad/entregaveis-emergentes-para-a-rede/raw/master/Figuras/Licen%C3%A7as/CC_BY-SA.88x31.png)

Author: **Cristthian Marafigo Arpino**