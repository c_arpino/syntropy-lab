Primeiramente pensamos em escrever um desses longos termos e condições aqui. O que é super chato de ler e escrever. Então para facilitar, fizemos uma versão curta: Tudo é gratuito para qualquer uso (comercial ou pessoal). Nós apreciaríamos se você mencionasse nosso nome em algum lugar. Ah, e você não pode nos processar se algo der errado. Por isso seja cuidadoso! Mas para aqueles que gostam de versões longas de termos e confições, podem conferir abaixo as licenças que utilizamos:

Autor: Cristthian Marafigo Arpino

## Avisos legais e licenciamento

- **CERN Open Hardware License v1.2**
    - Esta documentação descreve projetos de hardware aberto e livre. Os projetos de hardware estão licenciados sob termos da licença de hardware aberto do CERN OHL v.1.2. O que permite a liberdade do conhecimento para que qualquer um possa usar, estudar, modificar, distribuir, redistribuir e vender este projeto livremente, contanto que sigam os termos e condições da licença, como por exemplo, é vetada a possíbilidade de patentear o projeto ou projetos similares a este, saiba mais no texto da versão completa da licença acesse [CERN Open Hardware License v1.2](https://www.ohwr.org/projects/cernohl/wiki)
- **Creative Commons BY-SA 4.0**
    - O conteúdo desta página de wiki de projeto é distribuído sob uma licença [Creative Commons BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/). Autor: Cristthian Marafigo Arpino.
- **GPL GNU**
    - E os softwares desenvolvidos neste projeto para aplicações no Anemômetro e medições do potêncial eólico são disponibilizados sob a licença [GPL GNU](https://www.gnu.org/licenses/gpl.html).