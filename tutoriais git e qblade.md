# Documentação Git

Em primer lugar és necessario conferir se el git esta instalado en computer, si no basta
	sudo apt-get install git

Para Sincronizar o git com su computador és necessario usar
	git remote add origin https://tecnologias.libres.cc/c_arpino/syntrophy-lab.git
	git push origin master

Em seguida é preciso através do terminal entrar na pasta que você gostaria de clonar o repositorio git, por exemplos em /Downloads
	cd Downloads

Então clonamos o repositorio para nosso computador
	git clone https://tecnologias.libres.cc/c_arpino/syntrophy-lab.git
	

# Documentação QBlade

Para abrir el QBlade és necesario ir em programas
	cd /Programas
	cd QBlade\ 64bit\ release\ linux/

Después puede conferir se él inicializador del QBlade está em lá pasta e por fim abri-lo
	ls
	./run_qblade.sh

