# Micro Aerogeradores

[Português]

Desenvolvimento de projetos de hardware aberto e livre para microgeração de energia eólica. Este projeto tem como objetivo a documentação de projetos de micro aerogeradores horizontais e verticais.

---

[Castellano]

Desarrollo de proyectos de hardware libre y abierto para microgeneración de energía eólica. Este proyecto tiene como objetivo documentar el diseño de micro turbinas eólicas horizontales y verticales.

---

[English]

Development of free and open hardware projects for wind power microgeneration. This project aims to document the design of horizontal and vertical micro wind turbines.

---

**Contatos/Contactos/Contacts:**

- Cristthian Marafigo Arpino - cristthian.m.arpino@protonmail.com