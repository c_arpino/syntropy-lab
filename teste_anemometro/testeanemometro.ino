int valor=0;    //valor dado pelo sensor de direção
bool vel=false; //valor dado pelo sensor de velocidade.

void setup() 
{
  pinMode(A0, INPUT); //confiugração das portas como entrada. 
  pinMode(2, INPUT_PULLUP);
  Serial.begin(9600); //inicialização da serial
}

void loop() 
{
   vel=digitalRead(2);   //lê o sensor de velocidade
   valor=analogRead(A0); //lê o sensor de direção
   Serial.print(vel);    //printa os valores na serial
   Serial.print(" ");
   Serial.print(valor);
   Serial.print("\n");
   delay(20);            //realiza um delay de 20mseg (taxa de amostragem)

}
