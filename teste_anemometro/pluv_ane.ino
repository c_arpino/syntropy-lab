/*
**********************************************************
*        ANEMÔMETRO E PLUVIÔMETRO - CTA                  *
**********************************************************
 AUTOR: Béuren F. Bechlin

 O algoritmo desenvolvido para a operação de produto com-
prado na Sparkfun.

https://www.sparkfun.com/products/8942

A eletrônica necessária para o projeto está documentada em
um arquivo que deve estar juntamente a esse arquivo.

Esse algoritmo leva base o algoritmo que pode ser encontra-
do no seguinte site:

http://air.imag.fr/mediawiki/index.php/SEN-08942

       +-------------------------------------------+
       |   Centro de Tecnologia Acadêmica - UFRGS  |
       |         http://cta.if.ufrgs.br            |
       +-------------------------------------------+

 Este arquivo fonte faz parte do Pluviometro e Anemômetro
e está sobre as seguinte/s licença/s:
* GPL v3
*/

// CONSTANTES DE CALIBRAÇÃO
// 1 rev/segundo = 1.492 mph = 2.40114125 kph
#define CTE_CAL_ANEMOMETRO 2.4011
// 1 batida = 0.2794 mm
#define CTE_CAL_PLUVIOMETRO 0.2794


// Período entre as medidas em milisegundo
#define PERIODO_ANEMOMETRO  5000
#define PERIODO_DIR_VENTO   5000
#define PERIODO_PLUVIOMETRO  5000

// Pinos para conexão com Arduino
#define ANEMOMETRO_PIN   2     // Digital 2
#define PLUVIOMETRO_PIN  3     // Digital 3
#define DIR_VENTO_PIN    5     // Analog 5

// Variáveis para incrementação
volatile int numRevsAnemometro = 0;
volatile int numBatidasBascula = 0;

// Variáveis para realização do polling
unsigned long proximaMedidaAnemometro = 0;
unsigned long proximaMedidaPluviometro = 0;
unsigned long proximaMedidaDirVento = 0;
unsigned long tempo = 0;

// Direção do vento, valores de leitura para diferenciar cada direção:
// int adc[8] = {26, 45, 77, 118, 161, 196, 220, 256};
int adc[8] = {104, 180, 308, 472, 644, 784, 880, 1024};
// Relação entre os valores analógicos lidos e o que eles representam
// Para facilitar pode-se usar a biblioteca String
char *direcoes[8] = {"W","NW","N","SW","NE","S","SE","E"};
int direcaoInicial = 0;

void setup() {
   Serial.begin(9600);
   pinMode(ANEMOMETRO_PIN, INPUT);
   pinMode(PLUVIOMETRO_PIN, INPUT);
   digitalWrite(ANEMOMETRO_PIN, HIGH);
   digitalWrite(PLUVIOMETRO_PIN, HIGH);
   attachInterrupt(0, contadorAnemometro, FALLING);
   attachInterrupt(1, contadorPluviometro, FALLING);
}

void loop() {
    // Realizando o polling
   tempo = millis();

   if (tempo >= proximaMedidaAnemometro) {
      Serial.print("Vento (km/h): ");Serial.println(calcVelocidadeVento(), 2);
      proximaMedidaAnemometro = tempo + PERIODO_ANEMOMETRO;
   }
   if (tempo >= proximaMedidaDirVento) {
      Serial.print("Direcao: ");Serial.println(calcDirecaoVento());
      proximaMedidaDirVento = tempo + PERIODO_DIR_VENTO;
   }
   if (tempo >= proximaMedidaPluviometro) {
      Serial.print("Chuva (mm): ");Serial.println(calcQuantidadeChuva(), 3);
      proximaMedidaPluviometro = tempo + PERIODO_PLUVIOMETRO;
   }
}

/*
    Funções de callback de interrupção
*/
void contadorAnemometro() {
   numRevsAnemometro++;
}

void contadorPluviometro() {
   numBatidasBascula++;
}

double calcVelocidadeVento(){
   double velocidadeMedia;

   velocidadeMedia = numRevsAnemometro;
   velocidadeMedia *= 1000.0*CTE_CAL_ANEMOMETRO;
   velocidadeMedia /= PERIODO_ANEMOMETRO;

   // Resetando contador de pulsos do anemometro
   numRevsAnemometro = 0;
   return velocidadeMedia;
}

char* calcDirecaoVento() {
   int valor, x;
   valor = analogRead(DIR_VENTO_PIN);

   for (x = 0; x < 8; x++) {
      if (adc[x] >= valor)
         break;
   }

   // Ajustando direção inicial
   x = (x + direcaoInicial) % 8;
   return direcoes[x];
}

double calcQuantidadeChuva(){
    double volumeMedio;

    volumeMedio = numBatidasBascula;
    volumeMedio *= 1000.0*CTE_CAL_PLUVIOMETRO;
    volumeMedio /= PERIODO_PLUVIOMETRO;

    numBatidasBascula = 0;
    return volumeMedio;
}
